/*
 * List.hpp
 * Created by Andy Rash using information from the following sources:
 *
 * http://www.cplusplus.com/forum/articles/31828/
 *
 * http://www.cplusplus.com/forum/beginner/66054/
 *
 * http://www.panix.com/~elflord/cpp/list_howto/
 *
 * */

#ifndef LIST_HPP
#define LIST_HPP

#include <iostream>
#include <iterator>

template<class T>
class List {

private:

	// define a node	
	struct Node {
		
		Node(const T& data, Node * next = 0) : _data(data), _next(next) { }
		T _data;
		Node * _next;

	};

	// declare the head of the list,
	// the size, and the ID
	Node * _head = nullptr;;
	int _size = 0;
	std::string _ID;
	std::string _category;

public:

	// make the (non-const) iterator
	class iterator : public std::iterator<std::forward_iterator_tag, T> {
		
		// reference node
		Node * _ref = nullptr;

	public:

		friend class const_iterator;
		friend class List;

		// main iterator constructor
		inline iterator(Node * _node = 0) : _ref(_node) {}
		
		// iterator copy constructor
		inline iterator(const iterator& itr) : _ref(itr._ref) {}
	
		// overload assignment operator
		inline iterator& operator= (const iterator& itr) {
			
			_ref = itr._ref;

			return *this;

		}

		// overload pre-increment operator
		inline iterator& operator++ () {
			
			_ref = _ref->_next;

			return *this;

		}

		// overload post-increment operator
		inline iterator& operator++ (int) {
			
			iterator tmp(*this);

			tmp._ref = tmp._ref->_next;

			return tmp;

		}

		// overload the dereference operator
		inline typename List<T>::iterator::reference operator*() const { return _ref->_data; }
		
		// overload the arrow operator
		// (still can't get this to work for nested types... (i.e. List< List<T> >))
		inline typename List<T>::iterator::pointer operator->() const { return _ref->_data; }
		
		// overload the equality operator
		inline bool operator== (const iterator& itr) const { return _ref == itr._ref; }
		
		// overload the inequality operator
		inline bool operator!= (const iterator& itr) const { return _ref != itr._ref; }

	};

	// make the (non-const) iterator
	class const_iterator : public std::iterator<std::forward_iterator_tag, const T> {
		
		// reference node
		const Node * _ref = nullptr;

	public:

		friend class iterator;
		friend class List;
		
		// main const_iterator constructor
		inline const_iterator(const Node * _node = 0) : _ref(_node) {}
		
		// const_iterator copy constructor
		inline const_iterator(const const_iterator& itr) : _ref(itr._ref) {}
		
		// iterator/const_iterator copy constructor (i.e. const_iterator = iterator;)
		inline const_iterator(const iterator& itr) : _ref(itr._ref) {}
		
		// overload assignment operator (const_iterator)
		inline const_iterator& operator= (const const_iterator& itr) {
			
			_ref = itr._ref;
			return *this;

		}
		
		// overload assignment operator (iterator)
		inline const_iterator operator= (const iterator& itr) {
			
			_ref = itr._ref;
			return *this;

		}
		
		// overload pre-increment operator
		inline const_iterator& operator++ () {
			
			_ref = _ref->_next;
			return *this;

		}
		
		// overload post-increment operator
		inline const_iterator& operator++ (int) {
			
			const_iterator tmp(*this);
			_ref = _ref->_next;
			return tmp;

		}
		
		// overload the dereference operator
		inline typename List<T>::const_iterator::reference operator*() const { return _ref->_data; }
		
		// overload the arrow operator 
		// (still can't get this to work for nested types... (i.e. List< List<T> >))
		inline typename List<T>::const_iterator::pointer operator->() const { return _ref; }
		
		// overload the equality operator
		inline bool operator== (const const_iterator& itr) const { return _ref == itr._ref; }
		
		// overload the inequality operator	
		inline bool operator!= (const const_iterator& itr) const { return _ref != itr._ref;}

	};

	/*
	 * Constructors/destructors
	 *
	 * */

	// default constructor
	List();

	// copy constructor
	List(const List<T>& _list);	

	// destructor	
	~List();

	/*
	 * Member functions
	 *
	 * */

	// return the category of the List
	std::string& category();

	// clear the contents of the list
	void clear();	

	// return T/F if the list contains an item
	bool contains(const T& item) const;

	// return T/F if the list is empty
	bool empty() const;

	// erase after a certain point in the List	
	void erase_after(iterator& itr);

	// return the ID of the List	
	std::string& id();

	// insert a new element after a certain point
	// in the List	
	void insert_after(iterator& itr, const T& data);

	// pop first item in List	
	void pop_front();

	// push new item to the front of the List	
	void push_front(const T& data);

	// reverse the contents of the List
	void reverse();

	// set the List's category
	void set_category(const std::string& category);

	// set the List's ID	
	void set_id(const std::string& id);

	// return the size of the List	
	int size() const;

	// sort contents of the List	
	void sort();

	// swap contents of this List
	// and another
	void swap(List& _list);

	// overload assignment operator	
	List& operator= (const List& _list);	

	// return the front value
	inline T& front() { return *begin(); }
	
	// return the front value of a constant list
	inline const T& front() const { return *begin(); }

	// return pointer to first item in list
	inline iterator begin() { return iterator(_head); }
	
	// return pointer to last item in list
	inline iterator end() { return iterator(); }
	
	// return pointer to first item in constant list	
	inline const_iterator begin() const { return _head; }
	
	// return pointer to last item in constant list
	inline const_iterator end() const { return const_iterator(); }

	/*
	 * Non-member functions
	 *
	 * */

	// overload comparison operators	
	friend inline bool operator== (const List& lhs, const List& rhs) { 

		// this implementation comes directly from
		// the STL implementation of a forward_list	
		List<T>::const_iterator __ix = lhs.begin();
		List<T>::const_iterator __ex = lhs.end();
		List<T>::const_iterator __iy = rhs.begin();
		List<T>::const_iterator __ey = rhs.end();

		for(; __ix != __ex && __iy != __ey; ++__ix, ++__iy) {
			
			if(!(*__ix == *__iy)) {
				
				return false;

			}

		}

		return (__ix == __ex) == (__iy == __ey);

	}	

	friend inline bool operator!= (const List& lhs, const List& rhs) 
		{ return !(lhs == rhs); }

	friend inline bool operator< (const List& lhs, const List& rhs) 
		{ return lhs._ID < rhs._ID; }

	friend inline bool operator> (const List& lhs, const List& rhs) 
		{ return operator< (rhs,lhs); }

	friend inline bool operator<= (const List& lhs, const List& rhs) 
		{ return !operator> (lhs,rhs); }

	friend inline bool operator>= (const List& lhs, const List& rhs) 
		{ return !operator< (lhs,rhs); }

	// overload the stream insertion operator
	friend inline std::ostream& operator<< (std::ostream& os, const List<T>& _list) {
	
		Node * tmp = _list._head;

		while (tmp != nullptr) {
		
			if(tmp->_next == nullptr) {
			
				os << tmp->_data;

			} else {
			
				os << tmp->_data << ',' << ' ';

			}

			tmp = tmp->_next;

		}

		return os;

	}

};

/*
 * Constructors/destructors
 *
 * */

// default constructor
template<class T>
List<T>::List() : _head(0), _size(0) { 
	
	_category = "";

	_ID = empty;

}

// copy contructor
template<class T>
List<T>::List(const List& _list) : _head(0), _size(0) {
	
	_category = _list._category;

	_ID = _list._ID;

	for(const_iterator i = _list.begin(); i != _list.end(); ++i) {
		
		push_front(*i);

	}
	
	// uses the reverse() function
	// defined below
	reverse();

}

// destructor
template<class T>
List<T>::~List() { 
	
	// clear the contents of the list upon destruction
	clear(); 

}

/*
 * Member functions
 *
 * */

// return the category of the List
template<class T>
std::string& List<T>::category() {

	return _category;

}

// clear the contents of the list
template<class T>
void List<T>::clear() {
	
	while(!empty()) {
		
		pop_front();

	}

}

// returns T/F if the list contains an item
template<class T>
bool List<T>::contains(const T& item) const {

	Node * tmp = _head;

	while (tmp != nullptr) {
	
		if(tmp->_data == item) {
		
			return true;

		}

		tmp = tmp->_next;

	}

	return false;

}

// returns T/F if the list is empty
template<class T>
bool List<T>::empty() const { 

	return !_head; 

}

// erase after a certain point in the List
template<class T>
void List<T>::erase_after(iterator& itr) {
	
	Node * tmp = itr._ref->_next;
	
	if(itr._ref->_next) {
		
		itr._ref->_next = itr._ref->_next->_next;

	}

	delete tmp;
	--_size;

}

// return the ID of the list
template<class T>
std::string& List<T>::id() {
	
	return _ID;
		
}

// insert a new element after a certain point
// in the List
template<class T>
void List<T>::insert_after(iterator& itr, const T& data) {
	
	Node * tmp = new Node(data,itr._ref->_next);

	itr._ref->_next = tmp;

	++_size;

}

// pop first item in list
template<class T>
void List<T>::pop_front() {
	
	if(_head) {
		
		Node * newHead = _head->_next;
		delete _head;
		_head = newHead;

		--_size;

	}

}

// push new item to the front of the list
template<class T>
void List<T>::push_front(const T& data) {

	bool isDuplicate = false;
	const T& temp = data;

	for(List<T>::iterator itr = this->begin(); itr != this->end(); ++itr) {
		
		if(*itr == temp) {
			
			isDuplicate = true;
			break;

		}

	}

	if(isDuplicate == false) {
		
		Node * tmp = new Node(data);
		tmp->_next = _head;
		_head = tmp;

		++_size;

	}

}

// reverse the contents of the List
template<class T>
void List<T>::reverse() {
	
	Node * p = 0;
	Node * i = _head;
	Node * n;

	while(i) {
		
		n = i->_next;
		i->_next = p;
		p = i;
		i = n;

	}

	_head = p;
		
}

// set the List's category
template<class T>
void List<T>::set_category(const std::string& category) {

	_category = category;

}

// set the List's ID
template<class T>
void List<T>::set_id(const std::string& id) {
	
	_ID = id;

}

// return the size of the list
template<class T>
int List<T>::size() const {
	
	return _size;

}

// sort contents of list
template<class T>
void List<T>::sort() {
	
	Node * tmp = _head;
	T tmpData;
	int counter = 0;
	
	// get total number of elements in list
	while(tmp) {
		
		tmp = tmp->_next;
		counter++;

	}
	tmp = _head;

	for(int i = 0; i < counter; ++i) {
		
		while(tmp->_next) {
			
			if(tmp->_data > tmp->_next->_data) {
				
				tmpData = tmp->_data;
				tmp->_data = tmp->_next->_data;
				tmp->_next->_data = tmpData;

				tmp = tmp->_next;

			} else {
				
				tmp = tmp->_next;

			}

		}
		tmp = _head;

	}

}

// swap contents of this List
// and another
template<class T>
void List<T>::swap(List& _list) {
	
	Node * tmp = _head;
	_head = _list._head;
	_list._head = tmp;

	int tmpSize = _size;
	_size = _list._size;
	_list._size = tmpSize;

	std::string tmpCat = _category;
	_category = _list._category;
	_list._category = tmpCat;

	std::string tmpID = _ID;
	_ID = _list._ID;
	_list._ID = tmpID;

}

// overload assignment operator
template<class T>
List<T>& List<T>::operator=(const List<T>& _list) {
	
	// makes use of the copy constructor
	// and the swap() function
	// defined above
	List tmp(_list);
	swap(tmp);
	return *this;

}

/*
 * Non-member functions
 *
 * */

#endif

